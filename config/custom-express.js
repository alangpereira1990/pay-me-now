var express = require('express'); //camada que lida com as requisições http
var consign = require('consign'); //camada responsável por centralizar o carregamento de módulos
var bodyParser = require('body-parser'); //interpreta os parametros das requisições
var expressValidator = require('express-validator'); //validar parametros de entrada 

module.exports = () => {

    var app = express();

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());

    app.use(expressValidator());

    consign()
      .include('controllers')
      .then('persistencia')
      .into(app);

    return app;
}