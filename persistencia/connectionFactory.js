var mysql = require('mysql');

function createDBConnection(){

    console.log('criando conexão');

    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'bdpaymenow'
    });
}

module.exports = function() {
    return createDBConnection;
}