function createDAO(app){
    var connection = app.persistencia.connectionFactory();
    var pagamentoDAO = new app.persistencia.pagamentoDAO(connection);

    return pagamentoDAO;
}

module.exports = (app) => {

    

    app.get('/pagamentos', (req, res) => {

        res.send('OK');
    })

    app.post('/pagamentos/pagamento', (req, res) => {

        var pagamento = req.body;
        console.log('incluindo pagamento...');
        
        req.assert("forma_de_pagamento","forma de pagamento é obrigatório!").notEmpty();
        req.assert("valor","valor é obrigatório e deve ser um decimal!").notEmpty().isFloat();
        req.assert("moeda","moeda é obrigatório e deve contar 3 caracteres!").notEmpty().len(3,3);

        var erros = req.validationErrors();

        if (erros){
            console.log('Foram encontrados erros de validação');
            res.status(400).send(erros);
            return;
        }

        var pagamentoDAO = createDAO(app);

        pagamento.status = 'CRIADO';
        pagamento.data = new Date;
         
        pagamentoDAO.salva(pagamento, (exception, result) => {
            
            if (!exception){
                console.log('Pagamento criado: ' + JSON.stringify(result));

                pagamento.id = result.insertId;
                res.location('pagamentos/pagamento/' + result.insertId);
                res.status(201).json(pagamento);
            }
            else{
                console.log('Erro ao criar pagamento: ' + exception);
                res.status(400).send(exception);
            }
        });
    })
}
